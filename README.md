# Libreplan



## Getting started

This is a SoapUI project to test Libreplan REST API.
This include Project, Calendar and Resources API 


## Installation

All tests will be lauch by a Jenkins.
The deploy is currently manually, The tester must install hiw own instance of Libreplan. All endpoint are set on localhost:8180 that's why to use it as it is it MUST be set on port 8180. 

There is a script to interact with the data base in the test suite "Resource" , then you might need to change it to be working correctly with your own environnement. 
Here what was used to connect to the database : 
```
def driver = 'org.postgresql.Driver'
def url = 'jdbc:postgresql://localhost:5432/libreplan'
def username = 'libreplan'
def password = 'libreplan'
```

There is a properties.txt to show an example of a datafile that use the project , you can interact with it to inject your expected value , for example : 

```
project_name=ProjectNameCustom
project_code=ORDER0040
resource_code=ResourceOfYourChoice
```

Careful! project_code can't be change , it must remain ORDER0040 to not failed a specific assertion
AND do not change the key like "project_name" , only interact with the value "ProjectNameCustom"

This file must be at C:/test/test.txt (it also have to be named test.txt) , it can be change your if you set your own path in the script of EACH test suite setup.  


## Project status
Currently we have all this API automated as query by the client : 

•	Créer des projets
•	Supprimer des projets
•	Créer des ressources
•	Supprimer des ressources
•	Récupérer un projet
•	Récupérer tous les projets
•	Récupérer un calendrier
•	Récupérer tous les calendriers
•	Récupérer une ressource
•	Récupérer toutes les ressources

